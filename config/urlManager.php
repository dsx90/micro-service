<?php

return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'enableStrictParsing' => false,
    'rules' => [
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'manufacturer'
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'product'
        ],
        [
            'class' => 'yii\rest\UrlRule',
            'controller' => 'stock-product'
        ],
        'GET,POST <controller:text>/<id:\d+>/<action:update>' => '<controller>/<action>',
        'elfinder/<a>' => 'elfinder/<a>',
    ],
];