<?php

use kartik\grid\GridView;
use yii\helpers\Html;
use app\models\elastic\Organization;

/**
 * @var $this yii\web\View
 * @var \yii\elasticsearch\ActiveDataProvider $dataProvider
 * @var \app\models\search\ProductSearch $searchModel
 */

$this->title = 'Organizations';

?>
<div class="organization-index">
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'primaryKey',
            'name',
            'filePath',
            [
                'attribute' => 'updateTime',
                'content' => function(Organization $model){
                    if($model->updateTime){
                        return implode(", ", $model->updateTime);
                    }
                },
//                'filter' => \kartik\time\TimePicker::widget([
//                        'name' => 'OrganizationSearch[updateTime]'
//                    ])
            ],
            'checkDateTime:datetime',
            'updateDateTime:datetime',
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{update} {delete}',
            ],
        ]
    ])?>
</div>