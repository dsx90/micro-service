<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use app\models\StockProduct;
use kartik\grid\GridView;
use \yii\data\ArrayDataProvider;
use unclead\multipleinput\MultipleInput;
use kartik\time\TimePicker;

/**
 * @var $this \yii\web\View
 * @var $model \app\models\elastic\Organization
 */
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'class' => 'row'
    ]
]) ?>

<div class="col-md-12">
    <?= Html::a('Добавить в базу', ['create-products', 'id' => $model->primaryKey ], ['calss' => 'btn btn-primary'])?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>

<div class="col-md-9">
    <?= $form->field($model, 'id')->textInput() ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <div class="row">
        <div class="col-md-9">
            <?= $form->field($model, 'filePath')->textInput() ?>
            <?= $form->field($model, 'fileHash')->textInput()
                ->hint($model->updateDateTime ? $model->getAttributeLabel('updateDateTime').': '.Yii::$app->formatter->asDatetime($model->updateDateTime) : '') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'offset')->textInput() ?>
        </div>
    </div>
</div>
<div class="col-md-3">
    <?= $form->field($model, 'primaryKey')->textInput() ?>

    <?= $form->field($model, 'checkDateTime')->textInput() ?>

    <?= $form->field($model, 'updateTime')->widget(MultipleInput::class, [
        'allowEmptyList'    => false,
        'enableGuessTitle'  => true,
        'iconSource' => MultipleInput::ICONS_SOURCE_FONTAWESOME,
        'columns' => [
            [
                'name' => 'updateTime',
                'items' =>  $model->updateTime,
                'type'  => TimePicker::class,
                'options' => [
                    'pluginOptions' => [
                        'showMeridian' => false
                    ]
                ]
            ]
        ]
    ]) ?>
</div>

<?php
if($data = $model->generateStockProduct($model->filePath)){
    $dataProvider = new ArrayDataProvider([
        'allModels' => $data,
        'pagination' => [
            'pageSize' => 20,
        ],
    ]);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'pjax' => true,
        'columns' => (function() use ($model, $data){
            $keys = array_keys($data[0]);
            $columns = [
                ['class' => 'kartik\grid\SerialColumn']
            ];
            foreach($keys as $key){
                $columns[] = [
                    'attribute' => $key,
                    'header' => Html::dropDownList("Organization[fixedFields][$key]", $model->fixedFields[$key], StockProduct::getColumns(), [
                        'class' => 'form-control select-excel-field',
                        'prompt' => ''
                    ])
                ];
            }
            return $columns;
        })(),
        'options' => ['class' => 'col-md-12']
    ]);
}
?>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end() ?>

<?= $this->registerCss('
.multiple-input{
    margin-top: -12px;
}
')?>
