<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\elastic\Organization
 */

$this->title = $model->name;

?>
<div class="organization-update">
    <?= $this->render('_form', compact('model'))?>
</div>
