<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use app\models\elastic\StockProduct;

/**
 * @var $this \yii\web\View
 * @var $model \app\models\StockProduct
 */
?>

<?php $form = ActiveForm::begin([
    'class' => 'row'
]) ?>

<?= $form->field($model, 'id')->textInput() ?>

<?= $form->field($model, 'name')->textInput() ?>

<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end() ?>
