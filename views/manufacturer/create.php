<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\StockProduct
 */
?>
<div class="stock_product-create">
    <?= $this->render('_form', [
            'model' => $model
    ])?>
</div>