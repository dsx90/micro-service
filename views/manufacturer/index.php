<?php

use kartik\grid\GridView;
use app\models\elastic\Manufacturer;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * @var $this yii\web\View
 * @var $searchModel \app\models\search\ManufacturerSearch
 * @var $dataProvider \yii\elasticsearch\ActiveDataProvider
 */

$this->title = 'Manufacturers';

?>
<div class="manufacturer-index">
    <p>
        <?= Html::a('Новое предложение', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'primaryKey',
            'name',
            [
                'attribute' => 'aliases',
                'content' => function(Manufacturer $model){
                    return implode(", ", $model->aliases);
                }
            ],
            ['class' => 'kartik\grid\ActionColumn'],
        ]
    ])?>
</div>