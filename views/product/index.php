<?php

use app\models\elastic\Organization;
use kartik\grid\GridView;
use app\models\elastic\Product;

$this->title = 'Products';

/**
 * @var $this yii\web\View
 * @var \yii\elasticsearch\ActiveDataProvider $dataProvider
 * @var \app\models\search\ProductSearch $searchModel
 */
?>
<?php print_r(Product::find()->count());?>

<div class="product-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax'=>true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
            'primaryKey',
//            [
//                'attribute' => 'manufacturer.name',
//                'content' => function(Product $product){
//                    return $product->manufacturer->name;
//                }
//            ],
            'sid',
            'name',
//            [
//                'attribute' => 'count',
//                'content' => function(Product $model){
//                    return $model->getStockProducts()->count();
//                }
//            ],
            [
                'class' => 'kartik\grid\ActionColumn',
            ],
        ]
    ])?>
</div>