<?php

use app\models\elastic\Manufacturer;
use app\models\elastic\Stock;
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

/**
 * @var $this \yii\web\View
 * @var $model \app\models\elastic\Product
 */

$this->title = $model->name;
?>

<?php $form = ActiveForm::begin([
    'class' => 'row'
]) ?>

<div class="row">
    <div class="col-md-6">
        <?= $form->field($model, 'sid')->textInput() ?>
    </div>
    <div class="col-md-6">
        <?= $form->field($model->manufacturer, 'name')->textInput() ?>
    </div>
</div>

<?= $form->field($model, 'name')->textInput() ?>

<?= GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $model->stockProducts]),
    'pjax'=>true,
    'columns' => [
        ['class' => 'kartik\grid\SerialColumn'],
//            [
//                'attribute' => 'product',
//                'content' => function(StockProduct $model){
//                    return Html::a($model->product_id, ['product/update', ['id' => $model->elProduct->_id]]);
//                }
//            ],
        'amount',
        'price:currency',
        'stock.name',
        'stock.organization.name',
        ['class' => 'kartik\grid\ActionColumn'],
    ]
])?>

<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end() ?>
