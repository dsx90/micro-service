<?php

/**
 * @var $this \yii\web\View
 * @var $model \app\models\elastic\Product
 */
?>
<div class="stock_product-update">
    <?= $this->render('_form', compact('model'))?>
</div>
