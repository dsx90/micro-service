<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use app\models\elastic\StockProduct;

/**
 * @var $this \yii\web\View
 * @var $model \app\models\StockProduct
 */
?>

<?php $form = ActiveForm::begin([
    'class' => 'row'
]) ?>

<?= $form->field($model, 'sid')->textInput() ?>

<?= $form->field($model, 'name')->textInput() ?>

<?= $form->field($model, 'manufacturer')->textInput() ?>

<?= $form->field($model, 'price')->textInput() ?>

<?= $form->field($model, 'amount')->textInput() ?>

<?= $form->field($model, 'organization_id')->textInput() ?>

<?= $form->field($model, 'stockName')->textInput() ?>

<div class="form-group">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end() ?>
