<?php

/**
 * @var $this yii\web\View
 * @var $dataProvider \yii\elasticsearch\ActiveDataProvider
 * @var $searchModel \app\models\elastic\search\StockProductSearch
 */

use yii\helpers\Html;
use kartik\grid\GridView;

$this->title = 'Products';
 ?>

<p>
    <?= Html::a('Новое предложение', ['create'], ['class' => 'btn btn-success']) ?>
</p>

<div class="card site-index">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax'=>true,
        'columns' => [
            ['class' => 'kartik\grid\SerialColumn'],
//            [
//                'attribute' => 'product',
//                'content' => function(StockProduct $model){
//                    return Html::a($model->product_id, ['product/update', ['id' => $model->elProduct->_id]]);
//                }
//            ],
            'elProduct.sid',
            'elProduct.manufacturer.name',
            'elProduct.name',
            'name',
            'amount',
            'price:currency',
            'stock.name',
            'stock.organization.name',
            ['class' => 'kartik\grid\ActionColumn'],
        ]
    ])?>
</div>