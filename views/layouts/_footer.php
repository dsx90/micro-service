<?php

use yii\bootstrap4\Html;

?>

<footer class="main-footer">
    <strong>&copy; My Company <?= date('Y') ?> <?= Html::a(Yii::$app->name, Yii::$app->homeUrl)?>.</strong>

    <div class="float-right d-none d-sm-inline-block">
        <?= Yii::powered() ?>
    </div>
</footer>