<?php

use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;

?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <a href="<?= Yii::$app->homeUrl ?>" class="brand-link">
        <span class="brand-text font-weight-light"><?= Yii::$app->name?></span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <?php if(Yii::$app->user->isGuest): ?>

            <?php else:?>
                <div class="image">
                    <?= Html::img('dist/img/user2-160x160.jpg', ['class' => 'img-circle elevation-2', 'alt' => 'User Image'])?>
                </div>
                <div class="info">
                    <?= Html::a(Yii::$app->user->identity->username, '#', ['class' => 'd-block'])?>
                </div>
            <?php endif;?>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <?= Nav::widget([
                'options' => [
                    'class' => 'nav nav-pills nav-sidebar flex-column',
                    'data-widget' => 'treeview',
                    'role' => 'menu',
                    'data-accordion' => 'false'
                ],
                'items' => [
                    ['label' => 'Организации', 'url' => ['/organization/index']],
                    ['label' => 'Склады', 'url' => ['/stock/index']],
                    ['label' => 'Производители', 'url' => ['/manufacturer/index']],
                    ['label' => 'Продукты', 'url' => ['/product/index']],
                    ['label' => 'Проедложения', 'url' => ['/stock-product/index']],
                ],
            ])?>
        </nav>
    </div>
</aside>
