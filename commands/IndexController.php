<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 12.08.2019
 * Time: 9:33
 */

namespace app\commands;

use app\models\elastic\Manufacturer;
use app\models\elastic\Product;
use app\models\elastic\Organization;

use yii\console\Controller;
use yii\console\ExitCode;
use yii\db\Query;

ini_set('memory_limit', '800M');

class IndexController extends Controller
{
    /**
     * Индексация производителей
     *
     * @return int
     */
    public function actionManufacturer()
    {
        Manufacturer::createIndex();

        $manufacturers = (new Query())
            ->from('manufacturer')
            ->select([
                'manufacturer.id',
                'manufacturer.name'
            ])
            ->leftJoin('product', 'manufacturer.id = product.manufacturer_id')
            ->groupBy('manufacturer.id')
        ;

        $manufacturers = (new Query())
            ->select([
                'id',
                'name',
                'aliases' => 'json_agg(alias)'
            ])
            ->from(['m2' => $manufacturers])
            ->leftJoin('manufacturer_alias', 'm2.id = manufacturer_id')
            ->groupBy(['id', 'name'])
            //->limit(1)
        ;

        //echo $manufacturers->createCommand()->rawSql;

        $this->update($manufacturers, Manufacturer::class);

        return ExitCode::OK;
    }

    /**
     * @return int
     */
    public function actionProduct()
    {
        Product::deleteIndex();
        Product::createIndex();

        $products = (new Query())
            ->from('product')
            ->select(['id', 'manufacturer_id', 'sid', 'name'])
        ;

        $this->update($products, Product::class);

        return ExitCode::OK;
    }

    /**
     * @return int
     */
    public function actionOrganization()
    {
        Organization::deleteIndex();
        Organization::createIndex();
        $organization = (new Query())
            ->from('organization')
            ->where(['organization_type_id' => [1,2]])
            ->select(['id', 'name']);

        $this->update($organization, Organization::class);

        return ExitCode::OK;
    }

    private function update($data, string $className)
    {
        $models = [];
        $c = 0;
        foreach ($data->each(10000) as $model)
        {
            if($c == 1000){
                $className::createAll($models);
                $models = [];
                $c = 0;
            }
            $models[] = $model;
            $c++;
        }
//        if(!$className::get($model['id'])){
//            /** @var ActiveRecord $object */
//            $object = \Yii::createObject($className, [$model]);
//            $object->save();
//        };
    }
}