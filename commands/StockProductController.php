<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 15.08.2019
 * Time: 9:51
 */

namespace app\commands;


use app\models\elastic\Organization;
use app\models\StockProduct;
use yii\console\Controller;
use yii\console\ExitCode;

class StockProductController extends Controller
{
    /**
     * apt-get install cron
     *
     * crontab -e
     *
     * (* * * * * php /app/yii stock-product/generate)
     *
     * service cron start
     *
     * Логика:
     *      Найти время равное настоящему времени
     *      Найти все организации связанные с этим временем
     *      Проверить добавлен ли Excel документ и настроен ли он
     *      Занести все продукты с документа
     *
     * @return int
     * @throws \yii\base\InvalidConfigException
     */
    public function actionGenerate(){
        $time  = \Yii::$app->formatter->asTime(time(), "H:m");
        $time = '21:30';
        $organizations = Organization::find()
            ->filter([
                'term' => ['updateTime' => $time]
            ])
            ->limit(9999)
            ->all();

        foreach ($organizations as $organization){

            $organization->scenario = $organization::SCENARIO_CREATE_PRODUCTS;

            if($organization->filePath and $organization->fixedFields){
                foreach ($organization->generateStockProduct() as $item){
                    echo $organization->id.' - '.$organization->name."\n";
                    $fields = [
                        'organization_id' => $organization->id
                    ];
                    foreach ($item as $key => $value){
                        if($fixedKey = $organization->fixedFields[$key]){
                            $fields[$fixedKey] = $value;
                        }
                    }
                    $stockProduct = new StockProduct($fields);
                    $stockProduct->save();
                };
            }
        };
        return ExitCode::OK;
    }
}