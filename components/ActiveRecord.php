<?php

namespace app\components;

use yii\elasticsearch\Exception;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;

class ActiveRecord extends \yii\elasticsearch\ActiveRecord
{
    /**
     * @return string the name of the index this record is stored in.
     */
    public static function index()
    {
        return Inflector::camel2id(StringHelper::basename(get_called_class()), '-');
    }

    /**
     * @return string the name of the type of this record.
     */
    public static function type()
    {
        return '_doc';
    }

    /**
     * @param $models
     * @return mixed
     */
    public static function createAll($models)
    {
        $params = [];
        foreach ($models as $key => $model){
            $params[] = [
                'index' => [
                    '_index' => static::index(),
                    '_type' => static::type(),
                    '_id' => $model['id']
                ]
            ];
            $params[] = $model;
        }

        $bulkCommand = static::getDb()->createBulkCommand([
            "index" => static::index(),
            "type" => static::type(),
        ]);

        $bulkCommand->actions = $params;
        $response = $bulkCommand->execute();
        unset($response);
    }

    /**
     * Установка (update) для этой модели
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * Удалить индекс этой модели
     */
    public static function deleteIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->deleteIndex(static::index());
    }


}