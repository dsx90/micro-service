<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 12.08.2019
 * Time: 16:53
 */

namespace app\models\elastic;

use app\components\ActiveRecord;
use moonland\phpexcel\Excel;
use yii\helpers\ArrayHelper;

/**
 * Class Organization
 * @package app\models\elastic
 *
 * @property integer    $id
 * @property string     $name
 * @property string     $filePath           Путь к Excel файлу
 * @property integer    $offset             Отступ в Excel документе
 * @property array      $fixedFields        Соотношение полей к столбцам
 * @property array      $updateTime         Настройка времени проверки и получения Excel документа
 * @property string     $fileHash           Хеш последнего добавленного документа
 * @property string     $checkDateTime      Время проверки остатков
 * @property string     $updateDateTime     Время упешного обновления остатков
 */

class Organization extends ActiveRecord
{
    public $id;

    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    /** Сценарий обновления остатков */
    const SCENARIO_CREATE_PRODUCTS = 'create_products';

    /**
     * @return array список атрибутов для этой записи
     */
    public function attributes() : array
    {
        return [
            'name', 'filePath', 'offset',
            'fixedFields', 'fileHash', 'updateTime',
            'checkDateTime', 'updateDateTime'
        ];
    }

    /**
     * @return array
     */
    public function rules() : array
    {
        $baseRules = [
            ['name', 'required'],
            ['offset', 'integer'],
            [['name', 'filePath'], 'string'],
            [['fixedFields', 'checkTime'], 'safe'],
            ['fileHash', 'string'],
            ['updateTime', 'safe'],
        ];

//        $rules[self::SCENARIO_CREATE] = ArrayHelper::merge($baseRules, [
//            ['primaryKey', 'integer'],
//        ]);
//
//        $rules[self::SCENARIO_UPDATE] = ArrayHelper::merge($baseRules, [
//
//        ]);
//
        $rules[self::SCENARIO_CREATE_PRODUCTS] = ArrayHelper::merge($baseRules, [
            [['checkDateTime', 'updateDateTime'], 'safe']
        ]);

        return $baseRules;
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE_PRODUCTS] = [];
        $scenarios[self::SCENARIO_CREATE] = ['id','name'];
        return $scenarios;
    }

    /**
     * @return array
     */
    public function attributeLabels() : array
    {
        return [
            'primaryKey'        => 'ID',
            'name'              => 'Название',
            'filePath'          => 'Путь к файлу',
            'offset'            => 'Отступ',
            'fileHash'          => 'Хэш файла',
            'updateTime'        => 'Время проверки обновления',
            'checkDateTime'     => 'Вр-я пос-ей проверки остатков',
            'updateDateTime'    => 'Вр-я пос-го обновления',

        ];
    }

    /**
     * @return array Сопоставление для этой модели
     */
    public static function mapping()
    {
        return [
            'properties' => [
                'name'  => [
                    'type' => 'text'
                ],
                'filePath' => [
                    'type' => 'keyword',
                    'index' => false,
                ],
                'offset'  => [
                    'type' => 'integer',
                ],
                'fileHash' => [
                    'type' => 'keyword',
                    'index' => false,
                ],
                'updateTime'  => [
                    'type' => 'keyword',
                    'index' => false,
                ],
                'checkDateTime'  => [
                    'type' => 'date',
                    'index' => false,
                ],
                'updateDateTime'  => [
                    'type' => 'date',
                    'index' => false,
                ],
            ]
        ];
    }

    /**
     * Создать индекс этой модели
     */
    public static function createIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->createIndex(static::index(), [
            'settings' => [
                'analysis' => [
                    'filter' => [
                        'ru_stop' => [
                            'type' => 'stop',
                            'stopwords' => '_russian_'
                        ],
                        'ru_stemmer' => [
                            'type' => 'stemmer',
                            'language' => 'russian',
                        ]
                    ],
                    'analyzer' => [
                        'default' => [
                            'char_filter' => [
                                'html_strip'
                            ],
                            'tokenizer' => 'standard',
                            'filter' => [
                                'lowercase',
                                'ru_stop',
                                'ru_stemmer'
                            ]
                        ]
                    ]
                ]
            ],
            'mappings' => static::mapping(),

            //'warmers' => [ /* ... */ ],
            //'aliases' => [ /* ... */ ],
        ]);
    }

    public function beforeSave($insert) : bool
    {
        if (parent::beforeSave($insert)) {

            if($this->id){
                $this->primaryKey = $this->id;
            }

            if ($this->isAttributeChanged('filePath')){
                $this->fixedFields = null;
            }
            return true;
        }
        return false;
    }

    /**
     * Проверка последнего Хэш файла
     * Если хеш новый то мы записываем его
     * и даем доступ обработки файла
     *
     * @param $file
     * @return bool
     */
    public function checkHashFile($file) : bool
    {
        try {
            if (isset($file)) {
                if($this->scenario == self::SCENARIO_CREATE_PRODUCTS){
                    $hash = hash_file('md5', $file);
                    if ($this->fileHash == $hash) {
                        $this->updateAttributes([
                            'checkDateTime' => time()
                        ]);
                        return false;
                    } else {
                        $this->updateAttributes([
                            'fileHash' => $hash,
                            'updateDateTime' => time()
                        ]);
                        return true;
                    }
                }
                return true;
            }
        }
        catch (\Exception $e){
                echo $e;
                return false;
            }
        return false;
    }

    /**
     * @param null $file
     * @return array
     */
    public function generateStockProduct($file = null) : array
    {
        ini_set('memory_limit', '400M');

        $file = $file ?: $this->filePath ? \Yii::getAlias("@stock/$this->filePath") : null;

        $table = [];

        if($this->checkHashFile($file)){
            $excel = Excel::import($file, [
                'setFirstRecordAsKeys' => false
            ]);
            $table = array_slice($excel, $this->offset ?: null);
        }
        return $table;
    }
}