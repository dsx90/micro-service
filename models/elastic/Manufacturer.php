<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 12.08.2019
 * Time: 10:06
 */

namespace app\models\elastic;

use app\components\ActiveRecord;
use yii\helpers\Json;

/**
 * Class Manufacturer
 * @package app\models\elastic
 *
 * @property integer $id
 * @property string $name
 * @property string $aliases
 */
class Manufacturer extends ActiveRecord
{
    public $id;
    /**
     * @return array список атрибутов для этой записи
     */
    public function attributes()
    {
        return ['name', 'aliases'];
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            static::type() => [
                'properties' => [
                    'name'          => ['type' => 'string']
                ]
            ],
        ];
    }

    public function beforeSave($insert) : bool
    {
        if (parent::beforeSave($insert)) {

            $this->primaryKey = $this->id;

            $this->aliases = Json::decode($this->aliases);

            return true;
        }
        return false;
    }

    public function get_id()
    {
        return $this->primaryKey;
    }

    public function getProducts()
    {
        return $this->hasMany(Product::class, ['_id' => 'manufacturer_id']);
    }
}