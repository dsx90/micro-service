<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 12.08.2019
 * Time: 16:53
 */

namespace app\models\elastic;


use app\components\ActiveRecord;

/**
 * Class Stock
 * @package app\models\elastic
 *
 * @property  string    $name
 * @property  integer   $organization_id
 */

class Stock extends ActiveRecord
{
    public function attributes() : array
    {
        return ['organization_id', 'name'];
    }

    public function rules()
    {
        return [
            [['organization_id'], 'integer'],
            ['name', 'string']
        ];
    }

    public static function mapping()
    {
        return [
            static::type() => [
                '_parent' => [
                  'type' => Organization::type()
                ],
                'properties' => [
                    'name'  => ['type' => 'string'],
                    'organization_id' => ['type' => 'integer']
                ]
            ],
        ];
    }

    /**
     * @return \yii\db\ActiveQueryInterface
     */
    public function getOrganization()
    {
        return $this->hasOne(Organization::class, ['_id' => 'organization_id']);
    }
}