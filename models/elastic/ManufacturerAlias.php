<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 12.08.2019
 * Time: 10:06
 */

namespace app\models\elastic;

/**
 * Class ManufacturerAlias
 * @package app\models\elastic
 *
 * @property string $name
 */
class ManufacturerAlias extends Manufacturer
{
    /**
     * @return array список атрибутов для этой записи
     */
    public function attributes()
    {
        return ['name'];
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            'alias' => [
                'properties' => [
                    'name'      => ['type' => 'string']
                ]
            ],
        ];
    }
}