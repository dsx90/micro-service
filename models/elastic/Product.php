<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 12.08.2019
 * Time: 10:06
 */

namespace app\models\elastic;

use app\components\ActiveRecord;

/**
 * Class Product
 * @package app\models\elastic
 *
 * @property integer $id
 * @property string $name
 * @property string $sid
 * @property integer $manufacturer_id
 *
 * @property Manufacturer $manufacturer
 * @property StockProduct[] $stockProducts
 */
class Product extends ActiveRecord
{
    public $id;
    /**
     * @return array список атрибутов для этой записи
     */
    public function attributes()
    {
        return ['manufacturer_id', 'sid', 'name'];
    }

    /**
     * @return array
     */
    public function rules() : array
    {
        return [
            [['sid', 'manufacturer_id'], 'required'],
            [['sid', 'name'], 'string'],
            [['manufacturer_id'], 'integer'],
        ];
    }

    /**
     * @return array Сопоставление для этой модели
     */
    public static function mapping()
    {
        return [
            "properties" => [
                'id'                => ['type' => 'integer'],
                'manufacturer_id'   => ['type' => 'integer'],
                'sid'               => ['type' => 'keyword'],
                'name'              => ['type' => 'text'],
            ]
        ];
    }

    /**
     * Установка (update) для этой модели
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * Создать индекс этой модели
     */
    public static function createIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->createIndex(static::index(), [
            'mappings' => static::mapping()
        ]);
    }

    public function beforeSave($insert) : bool
    {
        if (parent::beforeSave($insert)) {

            if($this->id){
                $this->primaryKey = $this->id;
            }

            return true;
        }
        return false;
    }

    public function getManufacturer()
    {
        return $this->hasOne(Manufacturer::class, ['_id' => 'manufacturer_id']);
    }

    public function getStockProducts()
    {
        return $this->hasMany(StockProduct::class, ['product_id' => 'id']);
    }
}