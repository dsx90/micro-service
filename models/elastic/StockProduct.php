<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 12.08.2019
 * Time: 16:52
 */

namespace app\models\elastic;



use app\components\ActiveRecord;

/**
 * Class StockProduct
 * @package app\models\elastic
 *
 * @property string     $sid
 * @property string     $name
 * @property integer    $amount
 * @property integer    $minAmount
 * @property integer    price
 * @property integer    product_id
 * @property integer    stock_id
 *
 * @property \app\models\elastic\Stock      $stock
 * @property \app\models\elastic\Product    $elProduct
 * @property \app\models\database\Product   $dbProduct
 *
 */
class StockProduct extends ActiveRecord
{
    public $_id;
    /**
     * Поля для сохранения
     *
     * @return array
     */
    public function attributes() : array
    {
        return ['product_id', 'amount', 'price', 'stock_id', 'name', 'minAmount'];
    }


    /**
     * @return array
     */
    public function rules() : array
    {
        return [
            [['product_id'], 'required'],
            [['product_id', 'stock_id'], 'unique', 'targetAttribute' => ['product_id', 'stock_id']],
            ['product_id', 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => '_id']],
            ['stock_id', 'exist', 'skipOnError' => true, 'targetClass' => Stock::class, 'targetAttribute' => ['stock_id' => '_id']],

            ['name', 'string'],
            [['amount', 'minAmount'], 'integer'],
            [[/*'sid', */'price'], 'safe'],
//            ['price', 'double'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels() : array
    {
        return [
            'sid'           => 'Артикул',
            'manufacturer'  => 'Производитель',
            'name'          => 'Название',
            'amount'        => 'Количесво',
            'price'         => 'Цена',
            'stock'         => 'Склад',
            'minAmount'     => 'Мин кол-во'
        ];
    }

    /**
     * @return \yii\db\ActiveQueryInterface
     */
    public function getElProduct()
    {
        return $this->hasOne(Product::class, ['_id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQueryInterface
     */
    public function getDbProduct()
    {
        return $this->hasOne(\app\models\database\Product::class, ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQueryInterface
     */
    public function getStock()
    {
        return $this->hasOne(Stock::class, ['_id' => 'stock_id']);
    }

    public function getOrganization()
    {
        return $this->hasOne(Stock::class, ['_id' => 'organization_id']);
    }
}