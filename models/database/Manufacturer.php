<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 12.08.2019
 * Time: 9:26
 */

namespace app\models\database;

use yii\db\ActiveRecord;

/**
 * Class Manufacturer
 * @package app\models
 *
 * @property integer $id
 * @property string $title
 */
class Manufacturer extends ActiveRecord
{
    public function rules()
    {
        return [
          ['id', 'integer']
        ];
    }

}