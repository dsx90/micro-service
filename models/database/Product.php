<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 07.08.2019
 * Time: 11:55
 */

namespace app\models\database;

class Product extends \yii\db\ActiveRecord
{
    /**
     * @return array
     */
    public function rules() : array
    {
        return [
          [['id', 'manufacturer_id', 'sid', 'status', 'date_added', 'date_modified'], 'integer'],
          [['slug', 'name', 'meta_title'], 'string'],
          [['description', 'meta_description', 'meta_keywords'], 'string']
        ];
    }
}