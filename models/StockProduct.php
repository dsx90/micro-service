<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 14.08.2019
 * Time: 11:21
 */

namespace app\models;


use app\models\elastic\{
    Manufacturer,
    Product,
    Stock,
    StockProduct as ElasticStockProduct
};
use yii\base\Model;
use yii\elasticsearch\ActiveRecord;

/**
 * Class StockProduct Обработка данных перед загрузкой в модель
 * @package app\models
 *
 * @property string     $sid                Артикул
 * @property string     $manufacturer       Название производителя
 * @property string     $name               Название продукта
 * @property integer    $amount             Остаток
 * @property integer    $price              Цена
 * @property string     $stockName          Название склада
 * @property integer    $organization_id    Название организации
 *
 * @property Stock      $stock
 * @property Product    $product
 * @property Manufacturer $brand
 */
class StockProduct extends Model
{
    public $sid;
    public $manufacturer;
    public $name;
    public $price;
    public $amount;
    public $stockName;
    public $organization_id;
    public $minAmount;

    /**
     * Поля для сохранения с Excel таблицы
     *
     * @return array
     */
    public static function getColumns() : array
    {
        return [
            'sid'           => 'Артикул',
            'manufacturer'  => 'Производитель',
            'name'          => 'Название',
            'amount'        => 'Остаток',
            'price'         => 'Цена',
            'stockName'     => 'Склад',
            'minAmount'     => 'Мин кол-во'
        ];
    }

    /**
     * @return array
     */
    public function rules() : array
    {
        return [
            [['sid', 'manufacturer'], 'filter', 'filter' => function ($value) {
                return mb_convert_case($value, MB_CASE_LOWER);
            }],

            [['sid', 'manufacturer', 'organization_id'], 'required'],
            [[/*'sid', */'manufacturer', 'name'], 'string'],
            [['amount', 'minAmount'], 'integer'],
            [['sid', 'price'], 'safe'],
            ['stockName', 'string']
        ];
    }

    /**
     * Создает новое предложение для продукта
     *
     * @return ElasticStockProduct|null
     */
    public function save()
    {
        if(!$this->validate()){
            return null;
        }
        try{
            $stockProduct = ElasticStockProduct::find()->where(['_id' => $this->product->primaryKey]);
            if($stockProduct->exists()){
                $stockProduct = $stockProduct->one();
            } else {
                $stockProduct = new ElasticStockProduct();
                $stockProduct->stock_id = $this->stock->primaryKey;
            }
            $stockProduct->product_id = $this->product->primaryKey;
            $stockProduct->name = $this->name;
            $stockProduct->amount = $this->amount;
            $stockProduct->minAmount = $this->minAmount;
            $stockProduct->price = $this->price;

        } catch (\Exception $e){
            echo '<pre>';
            echo $e;
            echo '</pre>';
            return null;
        }

        if(!$stockProduct->save()){
            print_r($stockProduct->product_id);
            return print_r($stockProduct->errors);
        }
        return $stockProduct->save() ? $stockProduct : null;
    }

    /**
     * Получить производителя по названию или алиасу
     *
     * @return array|null|ActiveRecord
     */
    public function getBrand()
    {

        $manufacturer =  Manufacturer::find()
            ->where(['name' => $this->manufacturer])
            ->filter([
                'term' => ['aliases' => $this->manufacturer]
            ])
            //->orWhere(['aliases' => $this->manufacturer])
            ->one();

        return $manufacturer;
    }

    /**
     * Получить уникальный продукт по 2 полям
     *
     * @return array|null|ActiveRecord
     */
    public function getProduct()
    {
        $product = Product::findOne([
            'sid' => $this->sid,
            'manufacturer_id' => $this->brand->primaryKey
        ]);
        return $product;
    }

    /**
     * Получить склад
     * Если такого нет, то создать новый
     *
     * @return Stock|array|mixed|ActiveRecord|null
     */
    public function getStock()
    {
        $stock = Stock::find()->where(['name' => $this->stockName]);
        if(!$stock->exists()){
            $stock = new Stock();

            try {
                $stock->primaryKey = Stock::find()->orderBy(['_id' => SORT_ASC])->one()->primaryKey + 1;
            } catch (\Exception $e){
                $stock->primaryKey = 1;
            }

            $stock->organization_id = $this->organization_id;

            $stock->name = $this->stockName ?: 'Склад';
            $stock->save();

            return $stock;
        }
        return $stock->one();
    }
}