<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 13.08.2019
 * Time: 17:47
 */
namespace app\models\search;

use app\models\elastic\Manufacturer;
use yii\elasticsearch\ActiveDataProvider;

class ManufacturerSearch extends Manufacturer
{
    public function rules() : array
    {
        return [
            ['primaryKey', 'integer'],
            [['name', 'aliases'], 'string'],
            [['name', 'aliases'], 'filter', 'filter' => function ($value) {
                return mb_convert_case($value, MB_CASE_LOWER);
            }],
        ];
    }

    public function search($params)
    {
        $query = Manufacturer::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['_id' => $this->primaryKey]);
        $query->andFilterWhere(['name' => $this->name]);

        if($this->aliases){
            $query->filter([
                'term' => ['aliases' => $this->aliases]
            ]);
        }

        return $dataProvider;
    }
}