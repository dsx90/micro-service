<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 13.08.2019
 * Time: 17:47
 */
namespace app\models\search;

use app\models\elastic\StockProduct;
use yii\elasticsearch\ActiveDataProvider;
use yii\elasticsearch\ActiveQuery;

class StockProductSearch extends StockProduct
{
    /* @var $query ActiveQuery */
    public $query;

    public function rules() : array
    {
        return [];
    }

    public function search($params)
    {
        $query = StockProduct::find()
        ->with('elProduct')
        ->with('stock.organization')
        ;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}