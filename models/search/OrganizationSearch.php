<?php

namespace app\models\search;

use app\models\elastic\Organization;
use yii\elasticsearch\ActiveDataProvider;

class OrganizationSearch extends Organization
{
    public function rules() : array
    {
        return [
            ['primaryKey', 'integer'],
            [['name', 'updateTime'], 'string'],
            [['name'], 'filter', 'filter' => function ($value) {
                return mb_convert_case($value, MB_CASE_LOWER);
            }],
        ];
    }

    public function search($params)
    {
        $query = Organization::find();
        //$query->orderBy('_id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'primaryKey' => $this->primaryKey,
            'updateTime' => $this->updateTime
        ]);

        if($this->updateTime){
            $query->filter([
                'term' => ['updateTime' => $this->updateTime]
            ]);
        }

        // http://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-match-query.html
        if($this->name){
            $query->query([
//                "match" => ["name" => $this->name],
                'simple_query_string' => [
                    'query' => $this->name,
                    'fields' => [
                        "name^3",
//                        "tags^2",
//                        "content"
                    ]
                ]
            ]);

        }


        return $dataProvider;
    }
}