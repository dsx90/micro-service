<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 13.08.2019
 * Time: 17:47
 */
namespace app\models\search;

use app\models\elastic\Product;
use yii\elasticsearch\ActiveDataProvider;

class ProductSearch extends Product
{
    public function rules() : array
    {
        return [
            [['primaryKey', 'manufacturer_id'], 'integer'],
            [['name', 'sid'], 'string'],
            [['sid', 'name'], 'filter', 'filter' => function ($value) {
                return mb_convert_case($value, MB_CASE_LOWER);
            }],
        ];
    }

    public function search($params)
    {
        $query = Product::find()
            //->with('manufacturer')
        ;

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['primaryKey' => $this->primaryKey]);
        $query->andFilterWhere(['sid' => $this->sid]);

        if($this->name){
            $query->query([
                "match" =>  [
                    "content" => $this->name,
                ]
            ]);
        }

        return $dataProvider;
    }
}