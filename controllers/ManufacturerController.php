<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 12.08.2019
 * Time: 16:45
 */

namespace app\controllers;

use app\models\elastic\Manufacturer;
use Yii;
use app\models\search\ManufacturerSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ManufacturerController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new ManufacturerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $model = new Manufacturer();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save())
                Yii::$app->session->setFlash('success', 'Запись сохранена');
            return $this->render('update', [
                'model' => $model
            ]);
        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись обновлена');
            return $this->render('update', [
                'model' => $model
            ]);
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function actionDeleteAll()
    {
        Manufacturer::deleteIndex();
        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return Manufacturer|array|mixed|\yii\elasticsearch\ActiveRecord|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Manufacturer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}