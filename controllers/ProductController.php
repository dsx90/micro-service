<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 12.08.2019
 * Time: 16:45
 */

namespace app\controllers;

use app\models\search\ProductSearch;
use Yii;
use app\models\elastic\Product;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class ProductController extends Controller
{

    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись обновлена');
            return $this->render('update', [
                'model' => $model
            ]);
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    /**
     * @return \yii\web\Response
     */
    public function actionDeleteAll()
    {
        Product::deleteIndex();
        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return Product|array|mixed|null|\yii\elasticsearch\ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}