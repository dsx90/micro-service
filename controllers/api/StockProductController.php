<?php

namespace app\controllers\api;

use yii\rest\ActiveController;

class StockProductController extends ActiveController
{
    public $modelClass = 'app\models\elastic\StockProduct';
}