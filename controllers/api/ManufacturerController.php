<?php

namespace app\controllers\api;

use yii\rest\ActiveController;

class ManufacturerController extends ActiveController
{
    public $modelClass = 'app\models\elastic\Manufacturer';
}