<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 12.08.2019
 * Time: 16:45
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\elastic\StockProduct;
use app\models\StockProduct as StockProductForm;
use app\models\search\StockProductSearch;
use yii\web\NotFoundHttpException;


class StockProductController extends Controller
{
    public function actionIndex()
    {
        $searchModel = new StockProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $model = new StockProductForm();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->session->setFlash('success', 'Запись сохранена');
                return $this->render('update', [
                    'model' => $model
                ]);
            }
            else {
                echo '<pre>';
                print_r($model);
                echo '<pre>';
            }
        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    /**
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись обновлена');
            return $this->render('update', [
                'model' => $model
            ]);
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\elasticsearch\Exception
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionDeleteAll()
    {
        StockProduct::deleteIndex();
        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return StockProduct|array|mixed|null|\yii\elasticsearch\ActiveRecord
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = \app\models\elastic\StockProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}