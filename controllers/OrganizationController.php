<?php
/**
 * Created by PhpStorm.
 * User: ilchenko.m.a
 * Date: 12.08.2019
 * Time: 16:51
 */

namespace app\controllers;

use app\models\elastic\Organization;
use app\models\search\OrganizationSearch;
use app\models\StockProduct;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class OrganizationController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new OrganizationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * @return string
     */
    public function actionCreate()
    {
        $model = new Organization();
        $model->scenario = $model::SCENARIO_CREATE;

        if ($model->load(Yii::$app->request->post())) {
            if($model->save())
                Yii::$app->session->setFlash('success', 'Запись сохранена');
            return $this->render('update', [
                'model' => $model
            ]);
        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    /**
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        //$model->scenario = Organization::SCENARIO_UPDATE;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись обновлена');
            return $this->render('update', [
                'model' => $model
            ]);
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionCreateProducts($id)
    {
        $model = $this->findModel($id);

        $model->scenario = $model::SCENARIO_CREATE_PRODUCTS;

        foreach ($model->generateStockProduct() as $item){
            $fields = [
                'organization_id' => $model->primaryKey
            ];
            foreach ($item as $key => $value){
                if($fixedKey = $model->fixedFields[$key]){
                    $fields[$fixedKey] = $value;
                }
            }
            $stockProduct = new StockProduct($fields);

            if(!$stockProduct->save()){
//                echo '<pre>';
//                print_r($stockProduct);
//                return '</pre>';
            };
        };
        Yii::$app->session->setFlash('success', 'Продукты добавлены в базу');
        return $this->redirect(['update', 'id' => $id]);
    }

    /**
     * @param $id
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     * @throws \yii\elasticsearch\Exception
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * @return \yii\web\Response
     */
    public function actionDeleteAll()
    {
        Organization::deleteIndex();
        return $this->redirect(['index']);
    }

    /**
     * @param $id
     * @return Organization|array|mixed|\yii\elasticsearch\ActiveRecord|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = Organization::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}